/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	"fmt"
	"os"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

// log is for logging in this package.
var mrusnaklog = logf.Log.WithName("mrusnak-resource")

func (r *Mrusnak) SetupWebhookWithManager(mgr ctrl.Manager) error {
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

// TODO(user): EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!

//+kubebuilder:webhook:path=/mutate-goacademy-t-systems-com-v1-mrusnak,mutating=true,failurePolicy=fail,sideEffects=None,groups=goacademy.t-systems.com,resources=mrusnaks,verbs=create;update,versions=v1,name=mmrusnak.kb.io,admissionReviewVersions=v1

var _ webhook.Defaulter = &Mrusnak{}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (r *Mrusnak) Default() {
	mrusnaklog.Info("default", "name", r.Name)
	if r.Spec.HelmChartPath == "" {
		r.Spec.HelmChartPath = "/tmp/gin-webserver-chart-0.1.0.tgz"
	}

	// TODO(user): fill in your defaulting logic.
}

// TODO(user): change verbs to "verbs=create;update;delete" if you want to enable deletion validation.
//+kubebuilder:webhook:path=/validate-goacademy-t-systems-com-v1-mrusnak,mutating=false,failurePolicy=fail,sideEffects=None,groups=goacademy.t-systems.com,resources=mrusnaks,verbs=create;delete;update,versions=v1,name=vmrusnak.kb.io,admissionReviewVersions=v1

var _ webhook.Validator = &Mrusnak{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *Mrusnak) ValidateCreate() error {
	mrusnaklog.Info("validate create", "name", r.Name)

	// TODO(user): fill in your validation logic upon object creation.
	return nil
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *Mrusnak) ValidateUpdate(old runtime.Object) error {
	mrusnaklog.Info("validate update", "name", r.Name)
	// TODO(user): fill in your validation logic upon object update.
	file, err := os.Stat(r.Spec.HelmChartPath)
	if err != nil {
		return err
	}
	if !file.Mode().IsRegular() {
		return fmt.Errorf("%s isn't file", r.Spec.HelmChartPath)
	}
	return nil
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *Mrusnak) ValidateDelete() error {
	mrusnaklog.Info("validate delete", "name", r.Name)

	// TODO(user): fill in your validation logic upon object deletion.
	return nil
}
