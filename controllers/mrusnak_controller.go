/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"time"

	helmclient "github.com/mittwald/go-helm-client"
	v1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"

	goacademytsystemscomv1 "gitlab.com/michal.rusnak294/kube-operator/api/v1"
)

// MrusnakReconciler reconciles a Mrusnak object
type MrusnakReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

const myFinalizer = "michal-operator/finalizer"

//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=mrusnaks,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=mrusnaks/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=mrusnaks/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Mrusnak object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *MrusnakReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)
	logger.Info("Reconcile start")
	// TODO(user): your logic here
	var test goacademytsystemscomv1.Mrusnak
	err := r.Client.Get(ctx, req.NamespacedName, &test)
	if err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	opt := &helmclient.Options{
		Namespace: test.Spec.Namespace, // Change this to the namespace you wish the client to operate in.
		Debug:     true,
		Linting:   false,
		DebugLog:  func(format string, v ...interface{}) {},
	}

	helmClient, err := helmclient.New(opt)
	if err != nil {
		return ctrl.Result{}, err
	}

	chartSpec := helmclient.ChartSpec{
		ReleaseName: "gin-webserver-test",
		ChartName:   test.Spec.HelmChartPath,
		Namespace:   test.Spec.Namespace,
		Wait:        true,
		Timeout:     5 * time.Minute,
		ValuesYaml: `
image:
  pullPolicy: IfNotPresent
`,
	}

	logger.Info(chartSpec.ChartName)
	logger.Info(chartSpec.Namespace)

	if test.Spec.Restart != test.Status.Restart && test.Generation != 1 {

		var deployment v1.Deployment
		err = r.Client.Get(ctx, types.NamespacedName{Name: "gin-webserver-test", Namespace: test.Spec.Namespace}, &deployment)
		if err != nil {
			return ctrl.Result{}, err
		}

		if deployment.Spec.Template.ObjectMeta.Annotations == nil {
			deployment.Spec.Template.ObjectMeta.Annotations = make(map[string]string)
		}
		deployment.Spec.Template.ObjectMeta.Annotations["restartAt"] = time.Now().Format(time.RFC3339)
		err = r.Client.Update(ctx, &deployment)
		if err != nil {
			return ctrl.Result{}, err
		}
		patch := client.MergeFrom(test.DeepCopy())

		// Update the test status with the new restart value
		test.Status.Restart = test.Spec.Restart
		if err := r.Client.Status().Patch(ctx, &test, patch); err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, err
	}

	if test.DeletionTimestamp.IsZero() {
		logger.Info("INSTALL")
		// Install a chart release.
		// Note that helmclient.Options.Namespace should ideally match the namespace in chartSpec.Namespace.
		if _, err = helmClient.InstallOrUpgradeChart(ctx, &chartSpec, nil); err != nil {
			return ctrl.Result{}, err
		}

		patch := client.MergeFrom(test.DeepCopy())
		fmt.Println(test.Finalizers)

		if !controllerutil.ContainsFinalizer(&test, myFinalizer) {
			controllerutil.AddFinalizer(&test, myFinalizer)
			fmt.Println(test.Finalizers)
			if err := r.Patch(ctx, &test, patch); err != nil {
				return ctrl.Result{}, err
			}
		}
	} else {
		logger.Info("UNINSTALL")

		if err = helmClient.UninstallRelease(&chartSpec); err != nil {
			return ctrl.Result{}, err
		}

		patch := client.MergeFrom(test.DeepCopy())
		fmt.Println(test.Finalizers)

		if controllerutil.ContainsFinalizer(&test, myFinalizer) {

			controllerutil.RemoveFinalizer(&test, myFinalizer)
			fmt.Println(test.Finalizers)
			err := r.Patch(ctx, &test, patch)
			return ctrl.Result{}, err
		}

	}
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *MrusnakReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&goacademytsystemscomv1.Mrusnak{}).
		Complete(r)
}
